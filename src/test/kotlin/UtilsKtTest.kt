import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UtilsKtTest {
    @Test
    fun testSum() {
        val expected = 3
        val actual = sum(1 ,2)
        assertEquals(expected, actual)
    }

    @Test
    fun `test -3 equals -1 + -2`() {
        val expected = -3
        val actual = sum(-1 ,-2)
        assertEquals(expected, actual)
    }

    @Test
    fun `test 2 equals 1 + 1`() {
        assertEquals(2, sum(1, 1))
    }
}